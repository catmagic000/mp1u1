#include<iostream>
#include<cmath>
class Vector
{
public:
    Vector():_x(0.0),_y(0.0),_z(0.0){}
    Vector(float x, float y, float z):_x(x),_y(y),_z(z){}
    Vector operator+(const Vector& rhs)const
    {
        return Vector(this->_x+rhs._x, this->_y+rhs._y, this->_z+rhs._z);
    }
    Vector operator-(const Vector& rhs)const
    {
        return Vector(this->_x-rhs._x, this->_y-rhs._y, this->_z-rhs._z);
    }
    Vector operator*(double rhs)const
    {
        return Vector(this->_x*rhs, this->_y*rhs, this->_z*rhs);
    }
    friend Vector operator*(double lhs,const Vector& rhs);
    explicit operator float()const
    {
        return std::sqrt(_x*_x+_y*_y+_z*_z);
    }
    friend std::ostream& operator<<(std::ostream& out,const Vector& rhs);
    friend std::istream& operator>>(std::istream& out,Vector& rhs);
    float operator[](int index)
    {
        switch (index)
        {
            case 0: return _x;
            case 1: return _y;
            case 2: return _z;
            default:std::cerr<<"error";return 0;
        }
    }
private:
    float _x,_y,_z;
};
Vector operator*(double lhs,const Vector& rhs){return rhs*lhs;}
std::ostream& operator<<(std::ostream& out,const Vector& rhs)
{
    return out<<rhs._x<<" "<<rhs._y<<" "<<rhs._z;
}
std::istream& operator>>(std::istream& in,Vector& rhs)
{
    return in>>rhs._x>>rhs._y>>rhs._z;
}
